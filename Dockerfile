FROM peuybul/php-fpm-nginx:8.1.23-v1

RUN apt-get update

COPY . /var/www/html
COPY .config/php.ini /usr/local/etc/php/php.ini
COPY .config/nginx.conf /etc/nginx/nginx.conf
COPY .config/default.conf /etc/nginx/conf.d/default.conf

WORKDIR /var/www/html

EXPOSE 80

CMD service nginx start && php-fpm