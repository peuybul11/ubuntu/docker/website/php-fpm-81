# PHP FPM 8.1

## Running
```
docker-compose up --build -d
```

## Check Composer Version
```
docker exec -i phpfpm81 composer -v
```

## Check PHP Version
```
docker exec -i phpfpm81 php --version
```

PHP (Hypertext Preprocessor) adalah bahasa pemrograman skrip yang digunakan untuk mengembangkan situs web dinamis. PHP-FPM (PHP FastCGI Process Manager) adalah komponen tambahan untuk PHP yang bertanggung jawab untuk mengelola eksekusi skrip PHP melalui protokol FastCGI. Berikut adalah perbedaan antara PHP biasa dan PHP-FPM:

## Cara Eksekusi Skrip:
- PHP Biasa: Dalam pengaturan PHP biasa, skrip PHP dieksekusi secara inline, artinya setiap kali Anda mengakses halaman web yang memerlukan PHP, server web (seperti Apache atau Nginx) akan memproses skrip PHP secara langsung.
- PHP-FPM: PHP-FPM adalah manajer proses terpisah yang menjalankan skrip PHP secara terpisah dari server web. Ini berarti server web dan PHP-FPM berkomunikasi melalui protokol FastCGI. PHP-FPM mengelola antrian permintaan PHP dan memprosesnya secara terpisah.

# Kinerja:
- PHP Biasa: PHP biasa cenderung memiliki kinerja yang lebih rendah, terutama dalam skenario lalu lintas tinggi. Setiap permintaan PHP memerlukan proses yang berbeda oleh server web.
- PHP-FPM: PHP-FPM dirancang khusus untuk menangani lalu lintas tinggi dengan lebih efisien. Ini dapat mengelola banyak permintaan PHP dalam satu waktu, meminimalkan overhead dari perubahan proses yang berulang.

# Skalabilitas:
- PHP Biasa: Skrip PHP biasa kurang skalabel dalam situasi lalu lintas tinggi, karena setiap permintaan akan membuat proses PHP baru.
- PHP-FPM: PHP-FPM memungkinkan skalabilitas yang lebih baik, karena Anda dapat mengonfigurasi berapa banyak proses PHP yang harus dijalankan secara bersamaan. Ini memungkinkan Anda mengoptimalkan penggunaan sumber daya.

# Isolasi Proses:
- PHP Biasa: Dalam pengaturan PHP biasa, setiap permintaan PHP dieksekusi oleh proses yang berbeda, tetapi proses ini biasanya terkait dengan server web.
- PHP-FPM: Dengan PHP-FPM, setiap permintaan PHP dieksekusi dalam konteks proses PHP-FPM terpisah, yang dapat meningkatkan isolasi dan keamanan.

# Konfigurasi Terpisah:
- PHP Biasa: Konfigurasi PHP biasa sering kali tertanam dalam konfigurasi server web Anda, yang membuatnya sulit untuk mengelola berbagai versi PHP atau konfigurasi yang berbeda.
- PHP-FPM: PHP-FPM memungkinkan Anda untuk mengkonfigurasi setiap pool PHP secara terpisah, memungkinkan Anda mengelola berbagai aplikasi web dengan konfigurasi yang berbeda di dalam satu server.

Pilihan antara PHP biasa dan PHP-FPM tergantung pada kebutuhan dan lingkungan pengembangan atau produksi Anda. Dalam banyak kasus, PHP-FPM adalah pilihan yang lebih baik, terutama untuk aplikasi web dengan lalu lintas tinggi atau jika Anda ingin mengelola lebih baik penggunaan sumber daya server Anda.